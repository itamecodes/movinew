package com.itamecodes.moviepot.customviews;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

/**
 * Created by ananya on 3/8/14.
 */
public class AnimatedTextView extends TextView {
    private CharSequence mText;
    int mIndex=0;
    private long mDelay=500;
    private Animation in=new AlphaAnimation(0.0f,1.0f);

    public AnimatedTextView(Context context) {
        super(context);
    }

    public AnimatedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private Handler mHandler=new Handler();
    private Runnable characterAdder=new Runnable(){
        @Override
        public void run(){
            in.setDuration(2000);
            append(String.valueOf(mText.charAt(mIndex)));

            mIndex++;
           // startAnimation(in);
            if(mIndex<mText.length()){

                mHandler.postDelayed(characterAdder,mDelay);
            }
        }
    };
    public void animateText(CharSequence text){
        mText=text;
        mIndex=0;
        setText("");
        mHandler.removeCallbacks(characterAdder);
        mHandler.postDelayed(characterAdder,mDelay);
    }

    public void setCharacterDelay(long millis){
        mDelay=millis;
    }



}
