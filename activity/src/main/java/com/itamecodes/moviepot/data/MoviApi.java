package com.itamecodes.moviepot.data;

import retrofit.RestAdapter;

/**
 * Created by ananya on 3/8/14.
 */
public class MoviApi {
    private static final String MOVI_API_URL="https://api.themoviedb.org/3/movie";
    private static RestAdapter restAdapterMovie;

    public static RestAdapter getMovieRestAdapter(){
        if(restAdapterMovie==null){
            restAdapterMovie=new RestAdapter.Builder().setEndpoint(MOVI_API_URL).build();
        }
        return restAdapterMovie;
    }
}
