package com.itamecodes.moviepot.data.transforms;

import com.itamecodes.moviepot.data.model.Movie;
import com.itamecodes.moviepot.data.model.MovieList;

import java.util.List;

import rx.util.functions.Func1;
import timber.log.Timber;

/**
 * Created by ananya on 3/8/14.
 */
public class MovieListToListOfMovieTransform implements Func1<MovieList,List<Movie>> {
    @Override
    public List<Movie> call(MovieList movieList) {
        Timber.v("jingle");
        Timber.tag("movielist");
        Timber.v(movieList.results==null?"yes":"no");
        Timber.v(movieList.results.size()+"");
        return movieList.results;
    }
}
