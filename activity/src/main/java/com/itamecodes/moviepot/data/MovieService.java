package com.itamecodes.moviepot.data;

import com.itamecodes.moviepot.data.model.MovieList;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;


/**
 * Created by ananya on 3/8/14.
 */
public interface MovieService {
    @GET("/upcoming")
    Observable<MovieList> getUpcomingMovie(@Query("api_key") String apiKey);

}
