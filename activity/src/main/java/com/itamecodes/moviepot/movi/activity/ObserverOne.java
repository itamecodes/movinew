package com.itamecodes.moviepot.movi.activity;


import com.itamecodes.moviepot.data.model.Movie;

import rx.Observer;
import timber.log.Timber;

/**
 * Created by ananya on 3/9/14.
 */
public class ObserverOne implements Observer<Movie>{
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(Movie mov) {
        Timber.tag("observerone");
        Timber.v(mov.original_title);
    }
}
