package com.itamecodes.moviepot.movi.activity;

import android.app.Application;

import com.itamecodes.moviepot.BuildConfig;

import timber.log.Timber;

/**
 * Created by ananya on 3/8/14.
 */
public class MovieApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }
    }
}
