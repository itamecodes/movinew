package com.itamecodes.moviepot.movi.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.itamecodes.moviepot.R;
import com.itamecodes.moviepot.data.MoviApi;
import com.itamecodes.moviepot.data.MovieService;
import com.itamecodes.moviepot.data.model.Movie;
import com.itamecodes.moviepot.data.transforms.MovieListToListOfMovieTransform;

import java.util.List;

import hugo.weaving.DebugLog;
import retrofit.RestAdapter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.util.functions.Func1;
import timber.log.Timber;

/**
 * Created by ananya on 3/8/14.
 */
public class MainActivity extends ActionBarActivity {
    PublishSubject<Movie> publishSubject;

    @Override
    public void onCreate(Bundle icic){
        super.onCreate(icic);
        setContentView(R.layout.mainlayout);
        Timber.tag("Lifecycles");
        Timber.v("activity created");
        RestAdapter restAdapter=MoviApi.getMovieRestAdapter();
        MovieService movieService=restAdapter.create(MovieService.class);
        ObserverOne one=new ObserverOne();
        Observertwo two=new Observertwo();
        publishSubject=PublishSubject.create();
        publishSubject.subscribe(one);
        publishSubject.subscribe(two);
        movieService.getUpcomingMovie("b1a2d97608f210126674df510c71ab52")
                .map(new MovieListToListOfMovieTransform())
                .flatMap(new Func1<List<Movie>, Observable<Movie>>() {
                    @Override
                    public Observable<Movie> call(List<Movie> movies) {
                        Timber.tag("jingle");
                        Timber.v(movies.size()+"--"+movies.get(0).original_title);
                        return Observable.from(movies);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(publishSubject);
//                .subscribe(new Action1<Movie>() {
//            @Override
//            public void call(Movie movi) {
//                Timber.tag("awesome");
//                Timber.v(movi.id+"--"+movi.original_title);
//            }},new Action1<Throwable>() {
//                @Override
//                public void call(Throwable throwable) {
//                   Timber.v(""+ throwable.getLocalizedMessage()+throwable.getClass()+"");
//                    throwable.printStackTrace();
//                    // do something with the error
//                }
//                                                                     });


    }



    @DebugLog
    public int addInts(int ... a){
        int sum=0;
        for (int b:a){
            sum+=b;
        }
        return sum;
    }

}
